function Pokemon(name, lvl, hp) {
	this.name = name;
	this.level = lvl;
	this.hp = hp;
	this.attack = lvl;

	this.tackle = function(target) {
		if (this.hp > 10 && target.hp > 10) {
			console.log(this.name + " tackled " + target.name);
			target.hp = target.hp - this.attack 
			console.log(target.name + "'s health is now reduced to " + (target.hp));
			if (target.hp <= 10) {
				target.faint();
			}
		}	 
	};

	this.faint = function() {
		console.log(this.name + " fainted!");
	};
};

let torchic = new Pokemon("Torchic", 5, 50);
let treecko = new Pokemon("Treecko", 5, 40);

while (torchic.hp > 10 && treecko.hp > 10) {
	torchic.tackle(treecko);
	treecko.tackle(torchic);
};